
# PRUEBA TÉCNICA: DevSecOps 

API para registrar las visitas que tiene un inmueble en una tabla de DynamoDB, donde se registre la fecha y el lugar de la visita. 

## Demo

Por favor hacer clic en este link para ver un demo de la app y una explación de la prueba técnica: https://youtu.be/xWFPdGsDkIE

## Tech Stack

**Server:** AWS Lambda, Serverless Framework

**DB:** AWS DynamoDB

**CICD:** GitLab CICD

## API Reference

#### Get all items

```http
  GET /dev/visits
```

#### Get all items

```http
  POST /dev/visits
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `address` | `string` | **Required**. Direccion del inmueble |
| `startTime` | `string` | **Required**. Fecha de la visita |


