import type { AWS } from '@serverless/typescript';

const serverlessConfiguration: AWS = {
  service: 'habi-visits',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region: 'us-east-1',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
      VISITS_TABLE: 'visitsTable'
    },
    // iam role statement for dynamodb scan
    iam: {
      role: {
        statements: [
          {
            Effect: 'Allow',
            Action: ['dynamodb:Scan', 'dynamodb:PutItem'],
            Resource: 'arn:aws:dynamodb:${self:provider.region}:*:table/${self:provider.environment.VISITS_TABLE}',

          }
        ]
      }
    }
  },
  // import the function called handler via paths
  functions: { 
    getVisits:
      { handler: 'src/lambda/http/getVisits.handler',
        events: [
          {
            http: {
              method: 'get',
              path: 'visits', 
              cors: true
            }
          }
        ]
      },
    
    // function to create a visit
    createVisit:
      { handler: 'src/lambda/http/createVisit.handler',
        events: [
          {
            http: {
              method: 'post',
              path: 'visits',
              cors: true
            }
          }
        ] 
      }

  },
  // resource for dynamodb table
  resources: {
    Resources: {
      visitsTable: {
        Type: 'AWS::DynamoDB::Table',
        Properties: {
          TableName: 'visitsTable',
          AttributeDefinitions: [
            {
              AttributeName: 'visitId',
              AttributeType: 'S'
            }
          ],
          KeySchema: [
            {
              AttributeName: 'visitId',
              KeyType: 'HASH'
            }
          ],
          BillingMode: 'PAY_PER_REQUEST'
        }
      }
    }
  },


  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
  },
};

module.exports = serverlessConfiguration;
